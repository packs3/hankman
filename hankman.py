import random
import os 
from time import sleep

clear = lambda: os.system('clear')

# Отступы  ##########################

ots1 = '---------------------\n'
ots2 = '\n---------------------'

# Сложность  ########################

print(ots1, "Выберите сложность:", ots2)

dific = input("\n1. Легко \n2. Нормально \n3. Сложно \n4. HAHAHAHAHAHAHAHA...\n\nввод: ")

if dific == "1": attempt = 20
elif dific == "2": attempt = 10
elif dific == "3": attempt = 5
elif dific == "4": attempt = 1
sleep(1)
clear()
print(ots1, "Чтобы выйти из игры напишите - exit\nБонусы: Чтобы использовать бонус напишите - bonus", ots2)
sleep(2)
clear()
###############

usedln=""
win=[]


# Список  #####################
with open('words.txt', 'r') as w:
  for i in w:

# Рандомное слово  ############
    openFile = open('words.txt', 'r')
    listFile = list(openFile)
    randomWord = random.choice(listFile)

d1 ="".join(c for c in randomWord if c.isalpha())
randomWord = randomWord.rstrip('\n')
d1 = randomWord
d2=len(d1)
# print("слово: ",d1)

###############################   
# диапозон ##########
for i in range(len(randomWord)):
    win+="_"
 
iAttemts = True

while d2 != 0 and attempt != 0:
    test = False
######################

# Введите слово ##############
    while True:
        let = input("Введите букву: ")
##############################
# Выход из игры ##############
        if let == "exit":
            clear()
            print(ots1, "Вы успешно ВЫШЛИ из игры.", ots2)
            sleep(3)
            clear()
            quit()
# Бонус ########################

        if let == "bonus":
            clear()
            if iAttemts == True:
                print("Вы использовали аптечку!!!")
                attempt += 3
                iAttemts = False
                sleep(2)
                clear()
            elif iAttemts == False:
                print("Вы УЖЕ использовали аптечку!!!")
                sleep(2)
                clear()

###################################
        if let in usedln or len(let) > 1:
            clear()
            print(ots1,"Не больше одного символа, попробуйте снова!\nИспользованные буквы:", ' , '.join(usedln),ots2)
            print('[', win1, ']')
            print("Ваше здоровье = ", attempt)
        else:
            usedln += let
            break

    count = 0

    for i in randomWord:
      if let in i:
        d2 -= 1
        test = True
        win[count] = let
      count += 1

    if not test:
        attempt -= 1
        clear()
        print(ots1, "Неверно\nИспользованные буквы:", ' , '.join(usedln), ots2)
        win1 = ' '.join(win)
        print('[', win1, ']')
    else:
        clear()
        win1 = ' '.join(win)
        print(ots1, "Верно\nИспользованные буквы:", ' , '.join(usedln), ots2)
        print('[', win1, ']')

    print("Ваше здоровье = ", attempt)

if(d2 == 0):
    sleep(1)
    clear()
    print(ots1,"ВЫ ПОБЕДИЛИ!!! \n Слово было:", d1.upper(), ots2)
else:
    sleep(1)
    clear()
    print(ots1,"ВЫ ПРОИГРАЛИ! ПОПРОБУЙТЕ СНОВА!", "\n слово было: ", d1.upper(), ots2)